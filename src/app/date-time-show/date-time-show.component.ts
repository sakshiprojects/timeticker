import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { DateTimeServiceService } from './date-time-service/date-time-service.service';

@Component({
  selector: 'app-date-time-show',
  templateUrl: './date-time-show.component.html',
  styleUrls: ['./date-time-show.component.css'],
})
export class DateTimeShowComponent implements OnInit, OnDestroy {
  public dateTimeShow = '';
  subscriptionDateTime: Subscription = new Subscription();

  constructor(private dateTimeService: DateTimeServiceService) {}

  ngOnInit(): void {
    this.dateTimeDisplay();
  }

  dateTimeDisplay() {
    this.subscriptionDateTime = this.dateTimeService.dateTimeSub.subscribe(
      (dateTimeResult) => {
        this.dateTimeShow = dateTimeResult;
      }
    );
  }

  ngOnDestroy(): void {
    this.subscriptionDateTime.unsubscribe();
  }
}
