import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DateTimeServiceService {
  public dateTimeSub = new BehaviorSubject<string>('');
  public date = new Date();

  constructor() {
    setInterval(() => {
      this.dateTimeSub.next(this.displayDateTime());
    }, 1000);
  }

  displayDateTime() {
    this.date = new Date();
    const days: string[] = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const months: string[] = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    const dateTimeShow = `Today is 
       ${days[this.date.getDay()]}, ${this.date.getDate()} ${
      months[this.date.getMonth()]
    } ${this.date.getFullYear()},
     Time is ${this.date.getHours()}:${this.date.getMinutes()}:${this.date.getSeconds()}`;
    return dateTimeShow;
  }
}
